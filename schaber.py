#!/usr/bin/env python

import argparse
import logging
import re
import sys

from concurrent.futures import wait
from concurrent.futures.thread import ThreadPoolExecutor
from random import randrange
from time import sleep

from atomiclong import AtomicLong

import requests
import xmltodict

CHUNK_SIZE_MB = 16 * 1024 * 1024
SLEEP_TIME = 1
DATA_FILES_KEY = 'data_files'

actual_files_written_count = AtomicLong(0)


def main():
    args = get_command_line_args()
    setup_logging(logging.DEBUG if args.debug else logging.INFO)
    years = get_years_list(args)
    if years is not None:
        all_data_files_lists = get_files_list(args, years)
        if not download_files(args, all_data_files_lists):
            logging.critical('DOWNLOAD FAILED!')
            sys.exit(1)


def get_command_line_args():
    parser = argparse.ArgumentParser(description='Downloads Discogs data files')
    parser.add_argument('--debug', dest='debug', default=False, action='store_true', required=False,
                        help='Turns on debug mode')
    parser.add_argument('--dry-run', dest='dry_run', default=False, action='store_true', required=False,
                        help='Performs a dry run without downloading any data')
    parser.add_argument('--max-concurrent-downloads', type=int, default=1, required=False,
                        help='The maximum number of concurrent downloads')
    parser.add_argument('--output-folder', default='.', required=False, help='The pathname of the output folder')
    parser.add_argument('--type', dest='types', nargs='+', required=False, help='The types of data to download')
    parser.add_argument('--year', dest='years', nargs='+', required=False, help='The years to download the data for')
    args = parser.parse_args()
    return args


def setup_logging(level=logging.INFO):
    logging.basicConfig(level=level,
                        format='[%(asctime)s] %(levelname)s %(message)s')


def get_years_list(args):
    top_level_list_url = 'https://discogs-data.s3-us-west-2.amazonaws.com/?delimiter=/&prefix=data/'
    parsed_response = list_data(top_level_list_url)
    if parsed_response is not None:
        top_level_elements = parsed_response['ListBucketResult']['CommonPrefixes']
        years = [parse_year(element) for element in top_level_elements]
        return [filtered_year for filtered_year in filter(lambda year: args.years is None or year in args.years, years)]

    return None


def parse_year(element):
    search = re.search(r'^.*[^0-9]([1-9][0-9]{3})[^0-9].*', element['Prefix'])
    if search is not None:
        return search.group(1)
    else:
        logging.critical('{} did not have a valid year'.format(element['Prefix']))
    return None


def get_files_list(args, years):
    all_data_files = {}
    for year in years:
        data_files = get_data_files_list(args, year)
        all_data_files[year] = {DATA_FILES_KEY: data_files}
    return all_data_files


def download_files(args, all_data_files_dict):
    if args.max_concurrent_downloads < 2:
        return get_files_serial(args, all_data_files_dict)
    else:
        return get_files_concurrent(args, all_data_files_dict)


def get_files_serial(args, file_by_year_dict):
    expected_files_count = 0
    for year in file_by_year_dict.keys():
        file_names = file_by_year_dict[year]['data_files']
        for filename in file_names:
            expected_files_count += 1
            get_file(args, year, filename)

    if expected_files_count != actual_files_written_count.value:
        logging.critical('Not all files requested were downloaded')
        return False
    return True


def get_files_concurrent(args, file_by_year_dict):
    expected_files_count = 0
    with ThreadPoolExecutor(max_workers=args.max_concurrent_downloads) as executor:
        futures = []
        for year in file_by_year_dict.keys():
            for filename in file_by_year_dict[year]['data_files']:
                expected_files_count += 1
                future = executor.submit(get_file, args, year, filename)
                futures.append(future)

        wait(futures)

    if expected_files_count != actual_files_written_count.value:
        logging.critical('Not all files requested were downloaded')
        return False
    return True


def get_file(args, year, file_name):
    local_file_base_pathname, local_file_pathname = get_local_filename(args, year, file_name)
    logging.info('Downloading file {} to {}'.format(file_name, local_file_pathname))
    if args.dry_run:
        sleep_time = randrange(3, 7)
        logging.debug('Sleeping for {} seconds instead of downloading the file'.format(sleep_time))
        sleep(sleep_time)
    else:
        try:
            make_output_folder(local_file_base_pathname)
            download_file(file_name, local_file_pathname)
            global actual_files_written_count
            actual_files_written_count += 1
            sleep(SLEEP_TIME)  # Allow for garbage collection to take place
        except OSError as err:
            logging.critical('OS error: {}'.format(err))


def get_local_filename(args, year, web_file_name):
    search = re.search(r'^.*/discogs_([0-9]{8})_(.*)\.xml\.gz', web_file_name)
    if search is not None:
        output_folder_pathname = args.output_folder
        file_type = search.group(2)
        local_file_base_pathname = output_folder_pathname + '/' + file_type + '/' + year + '/'
        local_file_pathname = local_file_base_pathname + 'discogs_' + search.group(1) + '_' + file_type + '.xml.gz'
        return local_file_base_pathname, local_file_pathname
    else:
        logging.critical('{} did not have a valid local file name'.format(web_file_name))
    return None


def make_output_folder(output_folder):
    import pathlib
    pathlib.Path(output_folder).mkdir(parents=True, exist_ok=True)


def download_file(file_name, local_file_pathname):
    url_template = 'https://discogs-data.s3-us-west-2.amazonaws.com/{}'
    url = url_template.format(file_name)
    with requests.get(url, allow_redirects=True, stream=True) as response:
        logging.debug('Requesting file {}. Got {} from {}'.format(file_name, response.status_code, url))
        chunk_count = 0
        with open(local_file_pathname, 'wb') as file_handle:
            for chunk in response.iter_content(chunk_size=CHUNK_SIZE_MB):
                file_handle.write(chunk)
                chunk_count += 1
            logging.debug('File {} written in {} chunks'.format(local_file_pathname, chunk_count))


def get_data_files_list(args, year):
    url_template = 'https://discogs-data.s3-us-west-2.amazonaws.com/?delimiter=/&prefix=data/{}/'
    url = url_template.format(year)
    parsed_response = list_data(url)
    if parsed_response is not None:
        type_filter = re.compile('|'.join(args.types) if args.types is not None else '.*')
        contents = parsed_response['ListBucketResult']['Contents']
        filtered = filter(lambda item: item['Key'].endswith('.xml.gz') and type_filter.search(item['Key']), contents)
        return [entry['Key'] for entry in filtered]

    return None


def list_data(url):
    headers = {'user-agent': 'discogs/schaber'}

    response = requests.get(url, headers=headers)
    logging.debug('Got {} from {}'.format(response.status_code, url))
    if 200 <= response.status_code < 400:
        parsed_response = xmltodict.parse(response.text)
        return parsed_response
    else:
        logging.error('Got {} from {}'.format(response.status_code, url))
    return None


if __name__ == '__main__':
    main()

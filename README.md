# Downloading Discogs for fun and ... _well_ just for fun

### What?
This projects downloads data files from Discogs

### Why?
So that you can have fairly large data files to experiment with
Let's say you want to get some hand's on experience with
[Apache Spark](https://spark.apache.org), a lot of the tutorials
and examples out there use very limited data sets.

This tool lets you have a fairly large (from multi megabyte to multi
gigabyte) data files available locally for you to experiment with.

### How?


To download everything in the current directory as the script:
```bash
./schaber.py
```

To download everything to a selected location
```bash
./schaber.py --output /location/to/download/to
```

To get more details about what is going on:
```bash
./schaber.py --output-folder /location/to/download/to --debug
```

What command lines arguments are available:

| flag                       | description | examples |
| :------------------------- | :---------- | :------- |
| --debug                    | enables more detailed logging | `./schaber.py --debug` |
| --dry-run                  | simulates downloading of files without performing the actual download | `./schaber.py --dry-run` |
| --max-concurrent-downloads | sets the number of simultaneous downloads that take place | `./schaber.py --max-concurrent-downloads 7` |
| --output-folder            | sets the location of where the files will be downloaded. If the path does not exist it will be created | `./schaber.py --output-folder /location/to/download/to` |
| --type                     | set the type(s) of file to download (artists, labels, masters, releases) do not include to download all types | `./schaber.py --type labels`<br>`./schaber.py --type labels masters releases` |
| --year                     | set the year(s) to download do not include to download all years | `./schaber.py --year 2008`<br>`./schaber.py --year 2008 2010 2013 2017` |

For example to download data for 2020 for artists and labels using seven concurrent downloads to a specific location while increasing logging
```bash
./schaber.py --debug --output-folder /data_s/scrach/discogs --max-concurrent-downloads 7 --type artists labels --year 2020
``` 

### Notes
This tool demonstrates
- Command line argument parsing using the `argparse` module
- Making web/HTTP requests using the `requests` module. Also demonstrates how to download large files in a streaming fashion to conserve memory
- Multi threading using the `concurrent` module
- XML parsing using the `xmltodict` [module](https://github.com/martinblech/xmltodict)

# License
MIT

Copyright (c) 2020 Sukunkworx

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

